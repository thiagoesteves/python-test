# -*- coding: utf-8 -*-

import sys
import os
import json
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

# Import for the colored print
from colorama import init
init(strip=not sys.stdout.isatty()) # strip colors if stdout is redirected
from termcolor import cprint 
from pyfiglet import figlet_format

# Import for my simple library
import sample

json_string = '{"first_name": "monkey", "second_name":"pig", "third_name":"snake"}'
parsed_json = json.loads(json_string)

print("/\  "*10)
print("  \/"*10)

# You can call the function as string
cprint(figlet_format(parsed_json['first_name'], font='starwars'),
       'yellow', 'on_red', attrs=['bold'])
getattr(sample, parsed_json['first_name'])()

# you can call the function directly
cprint(figlet_format(parsed_json['second_name'], font='starwars'),
       'yellow', 'on_red', attrs=['bold'])
sample.pig()

cprint(figlet_format(parsed_json['third_name'], font='starwars'),
       'yellow', 'on_red', attrs=['bold'])
sample.snake()

print("/\  "*10)
print("  \/"*10)
