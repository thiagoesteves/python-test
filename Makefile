# Simple Makefile for project routines

init:
	pip install -r requirements.txt

clean:
	-rm -rf build
	-rm -rf *~*
	-find . -name '*.pyc' -exec rm {} \;

test:
	nosetests tests

