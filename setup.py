# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='first',
    version='0.0.1',
    description='My first project',
    long_description=readme,
    author='Thiago Esteves',
    author_email='calori@gmail.com',
    url='https://bitbucket.org/thiagoesteves/',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)

